# README #

### What is this repository for? ###

* Shader examples

### How do I get set up? ###

With pre-modeled deferred decals, the workflow is pretty simple.

* The project has to use deferred rendering.
* Make a mesh with basic texture mapping (e.g. map the surfaces to simple tiled textures)
* Make another mesh with quads and quad strips following the surface of the first one and mapped to a decal atlas texture (the project includes examples of meshes like those)
* Parent the latter mesh to the former and use a dedicated deferred decal shader to render it properly
* Shader allows you to tweak the output to deferred targets somewhat independently, allowing you to create stuff like normal-only decals

### Issues ###

* In Unity PBR implementation, GI is applied to emission before deferred shading, which makes it impossible to draw a normal-only decal receiving GI light on altered normals
* Albedo and specular outputs are deeply linked in PBR, so it makes no sense to decouple them into separately tweakable decal outputs, even though that's possible thanks to split deferred RTs
* Emissive output contains reflections, which are, again, deeply dependent on albedo, specular and smoothness outputs of the surface, so leaving it out is not recommended
* Due to use of alpha for smoothness, all shaders require a second pass where additive blending of smoothness for one of the RTs happens. No way around it, as far as I see.

### Who do I talk to? ###

* Feel free to contact me on the forums through my [profile](http://forum.unity3d.com/members/bac9-flcl.162520/)

### License ###

* License is basic MIT, full text included in the repo
* If you end up using anything, I'd appreciate a message showing your results - I'm always curious about other's results, that's how this whole thing got going (from looking at Star Citizen and Alien Isolation art workflows)

### Acknowledgements ###

The shaders wouldn't have been possible without extensive help from the following folks over at Unity Forums:

* Dolkar
* Marco Sperling