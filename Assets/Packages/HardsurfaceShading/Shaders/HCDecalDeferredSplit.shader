﻿Shader "Hardsurface/Decal/Deferred Split" 
{
	Properties 
	{
		_ColorMain ("Dielectric albedo/Metal specular", Color) = (1,1,1,1)
		_ColorEmission ("Emission color", Color) = (0,0,0,0)
		_ColorEmissionMultiplier ("Emission multiplier (HDR)", Range(1,16)) = 1.0
		
		_EmissionMap ("Emission map", 2D) = "black" {}
		_SmoothnessMap ("Smoothness map", 2D) = "black" {}
		_OcclusionMap ("Occlusion map", 2D) = "black" {}
		_OpacityMap ("Opacity map", 2D) = "black" {}
		_BumpMap ("Normal map", 2D) = "bump" {}

		_ParallaxMap ("Height map", 2D) = "white" {}
		_ParallaxHeight ("Parallax height", Range (0.005, 0.08)) = 0.02

		_SmoothnessLow ("Smoothness (Low)", Range(0,1)) = 0.5
		_SmoothnessHigh ("Smoothness (High)", Range(0,1)) = 0.5
		_Metalness ("Metalness", Range(0,1)) = 0.0

		_ContributionDiffuseSpecular ("Blend albedo/spec (RT0+1)", Range(0,1)) = 0.0
		_ContributionNormal ("Blend normals (RT2)", Range(0,1)) = 1.0
		_ContributionEmission ("Blend emission (RT3)", Range(0,1)) = 0.0

		_ContributionEmissionMask ("Output emission mask", Range(0,1)) = 0.0
		_ContributionOcclusion ("Output AO", Range(0,1)) = 1.0
		_ContributionCavity ("Output AO as cavity", Range(0,1)) = 1.0

		_MaskAlphaUsingAO ("Mask alpha with AO", Range(0,1)) = 0.0
		_MaskAlphaPower ("Mask alpha power", Range(1,4)) = 1.0
		_MaskAlphaShift ("Mask alpha shift", Range(0,1)) = 0.0
	}
	SubShader 
	{
		Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="Opaque" "ForceNoShadowCasting"="True"}
		LOD 300
		Offset -1, -1

		Blend SrcAlpha OneMinusSrcAlpha, Zero OneMinusSrcAlpha

		CGPROGRAM

		#pragma surface surf Standard finalgbuffer:DecalFinalGBuffer exclude_path:forward exclude_path:prepass noshadow noforwardadd keepalpha
		#pragma target 3.0

		sampler2D _EmissionMap;
		sampler2D _SmoothnessMap;
		sampler2D _OcclusionMap;
		sampler2D _OpacityMap;
		sampler2D _BumpMap;

		sampler2D _ParallaxMap;
		float _ParallaxHeight;

		struct Input 
		{
			float2 uv_OpacityMap;
			float3 viewDir;
		};

		fixed3 _ColorMain;
		fixed3 _ColorEmission;
		half _ColorEmissionMultiplier;
		half _Metalness;

		half _ContributionDiffuseSpecular;
		half _ContributionNormal;
		half _ContributionEmission;
		half _ContributionEmissionMask;
		half _ContributionOcclusion;
		half _ContributionCavity;

		half _MaskAlphaUsingAO;
		half _MaskAlphaPower;
		half _MaskAlphaShift;

		inline half ConvertToGrayscale (half3 c)
		{
			return dot (c, half3 (0.3, 0.59, 0.11));
		}

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			half h = ConvertToGrayscale (tex2D (_ParallaxMap, IN.uv_OpacityMap));
			float2 offset = ParallaxOffset (h, _ParallaxHeight, IN.viewDir);
			IN.uv_OpacityMap += offset;

			o.Alpha = ConvertToGrayscale (tex2D (_OpacityMap, IN.uv_OpacityMap));
			o.Occlusion = lerp (1, ConvertToGrayscale (tex2D (_OcclusionMap, IN.uv_OpacityMap)), _ContributionOcclusion);
			o.Albedo = _ColorMain * lerp (1, o.Occlusion, _ContributionCavity);
			o.Normal = UnpackNormal (tex2D (_BumpMap, IN.uv_OpacityMap));
			o.Metallic = _Metalness;
			o.Smoothness = 1 - o.Alpha;
			o.Emission = lerp (0, ConvertToGrayscale (tex2D (_EmissionMap, IN.uv_OpacityMap)) * _ColorEmission * _ColorEmissionMultiplier, _ContributionEmissionMask);
		}

		void DecalFinalGBuffer (Input IN, SurfaceOutputStandard o, inout half4 diffuse, inout half4 specSmoothness, inout half4 normal, inout half4 emission)
		{
			float alphaMultiplier = lerp (1, 1 - pow (saturate (o.Occlusion + _MaskAlphaShift), _MaskAlphaPower), _MaskAlphaUsingAO);

			diffuse.a = o.Alpha * alphaMultiplier * _ContributionDiffuseSpecular;
			specSmoothness.a = o.Alpha * alphaMultiplier * _ContributionDiffuseSpecular;
			normal.a = o.Alpha * _ContributionNormal; 
			emission.a = o.Alpha * alphaMultiplier * _ContributionEmission;
		}

		ENDCG

		Blend One One
		ColorMask A

		CGPROGRAM

		#pragma surface surf Standard finalgbuffer:DecalFinalGBuffer exclude_path:forward exclude_path:prepass noshadow noforwardadd keepalpha
		#pragma target 3.0

		sampler2D _OpacityMap;
		sampler2D _SmoothnessMap;
		sampler2D _OcclusionMap;
		sampler2D _BumpMap;

		sampler2D _ParallaxMap;
		float _ParallaxHeight;

		struct Input 
		{
			float2 uv_OpacityMap;
			float3 viewDir;
		};

		half _SmoothnessLow;
		half _SmoothnessHigh;
		half _ContributionDiffuseSpecular;
		half _ContributionOcclusion;
		
		half _MaskAlphaUsingAO;
		half _MaskAlphaPower;
		half _MaskAlphaShift;

		inline half ConvertToGrayscale (half3 c)
		{
			return dot (c, half3 (0.3, 0.59, 0.11));
		}

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			half h = ConvertToGrayscale (tex2D (_ParallaxMap, IN.uv_OpacityMap));
			float2 offset = ParallaxOffset (h, _ParallaxHeight, IN.viewDir);
			IN.uv_OpacityMap += offset;

			o.Alpha = ConvertToGrayscale (tex2D (_OpacityMap, IN.uv_OpacityMap));
			o.Occlusion = lerp (1, ConvertToGrayscale (tex2D (_OcclusionMap, IN.uv_OpacityMap)), _ContributionOcclusion);
			o.Smoothness = saturate (lerp (_SmoothnessLow, _SmoothnessHigh, ConvertToGrayscale (tex2D (_SmoothnessMap, IN.uv_OpacityMap))));
		}

		void DecalFinalGBuffer (Input IN, SurfaceOutputStandard o, inout half4 diffuse, inout half4 specSmoothness, inout half4 normal, inout half4 emission)
		{
			float alphaMultiplier = lerp (1, 1 - pow (saturate (o.Occlusion + _MaskAlphaShift), _MaskAlphaPower), _MaskAlphaUsingAO);

			specSmoothness.a *= o.Alpha * alphaMultiplier * _ContributionDiffuseSpecular;
		}

		ENDCG
	} 
	FallBack "Diffuse"
}