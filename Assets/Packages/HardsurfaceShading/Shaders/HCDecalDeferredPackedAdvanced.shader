﻿Shader "Hardsurface/Decal/Deferred Packed Advanced" 
{
	Properties 
	{
		_PackedMapA ("Main mask (R)/Secondary mask (G)/Smoothness (B)/AO (A)", 2D) = "white" {}
		_PackedMapB ("Emission (R)/Height (A)", 2D) = "white" {}
		_BumpMap ("Normal map", 2D) = "bump" {}

		_ColorMain ("Dielectric albedo/Metal specular (main)", Color) = (0.5,0.5,0.5,1)
		_ColorDirt ("Dielectric albedo/Metal specular (dirt)", Color) = (0.5,0.5,0.5,1)
		_ColorEmission ("Emission color", Color) = (0,0,0,0)
		_ColorEmissionMultiplier ("Emission multiplier (HDR)", Range(1,16)) = 1.0
		_ParallaxHeight ("Parallax height", Range (0.005, 0.08)) = 0.02

		_SmoothnessLow ("Smoothness (Low)", Range(0,1)) = 0.5
		_SmoothnessHigh ("Smoothness (High)", Range(0,1)) = 0.5
		_Metalness ("Metalness", Range(0,1)) = 0.0

		_ContributionDiffuseSpecular ("Blend albedo/spec (RT0+1)", Range(0,1)) = 0.0
		_ContributionNormal ("Blend normals (RT2)", Range(0,1)) = 1.0
		_ContributionEmission ("Blend emission (RT3)", Range(0,1)) = 0.0

		_ContributionEmissionMask ("Output emission mask", Range(0,1)) = 0.0
		_ContributionOcclusion ("Output AO", Range(0,1)) = 1.0
		_ContributionCavity ("Output AO as cavity", Range(0,1)) = 1.0

		_DirtIntensity ("Dirt intensity", Range(0,1)) = 1.0
		_DirtMaskAORemappingMin ("Dirt from AO mask remapping minimum", Range(0,0.5)) = 0.2
		_DirtMaskAORemappingMax ("Dirt from AO mask remapping maximum", Range(0.5,1)) = 0.8
	}
	SubShader 
	{
		Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="Opaque" "ForceNoShadowCasting"="True"}
		LOD 300
		Offset -1, -1

		Blend SrcAlpha OneMinusSrcAlpha, Zero OneMinusSrcAlpha

		CGPROGRAM

		#pragma surface surf Standard finalgbuffer:DecalFinalGBuffer exclude_path:forward exclude_path:prepass noshadow noforwardadd keepalpha
		#pragma target 3.0

		sampler2D _PackedMapA;
		sampler2D _PackedMapB;
		sampler2D _BumpMap;

		struct Input 
		{
			float2 uv_PackedMapA;
			float3 viewDir;
		};

		fixed3 _ColorMain;
		fixed3 _ColorDirt;
		fixed3 _ColorEmission;
		half _ColorEmissionMultiplier;

		float _ParallaxHeight;
		half _Metalness;

		half _ContributionDiffuseSpecular;
		half _ContributionNormal;
		half _ContributionEmission;
		half _ContributionOcclusion;
		half _ContributionCavity;

		half _DirtIntensity;
		half _DirtMaskAORemappingMin;
		half _DirtMaskAORemappingMax;

		inline half3 RemapColorToRange (half3 f, half min, half max)
		{
			f = saturate (f);
			f = (f - min) / (max - min);
			return f;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			half h = tex2D (_PackedMapB, IN.uv_PackedMapA).w;
			float2 offset = ParallaxOffset (h, _ParallaxHeight, IN.viewDir);
			IN.uv_PackedMapA += offset;

			fixed4 packedMapA = tex2D (_PackedMapA, IN.uv_PackedMapA);
			fixed4 packedMapB = tex2D (_PackedMapB, IN.uv_PackedMapA);
			fixed3 normal = UnpackNormal (tex2D (_BumpMap, IN.uv_PackedMapA));

			half remappedOcclusion = RemapColorToRange (packedMapA.w, _DirtMaskAORemappingMin, _DirtMaskAORemappingMax);
			half dirtMask = saturate (packedMapA.y + (1 - remappedOcclusion));

			o.Alpha = lerp (packedMapA.y, dirtMask, _DirtIntensity);
			o.Albedo = lerp (_ColorDirt, _ColorMain, packedMapA.y) * lerp (1, packedMapA.w, _ContributionCavity);
			o.Normal = normal;
			o.Metallic = _Metalness;
			o.Smoothness = 1 - o.Alpha;
			o.Occlusion = lerp (1, packedMapA.w, _ContributionOcclusion);
			o.Emission = lerp (0, dirtMask * _ColorEmission * _ColorEmissionMultiplier, _ContributionEmission);
		}

		void DecalFinalGBuffer (Input IN, SurfaceOutputStandard o, inout half4 diffuse, inout half4 specSmoothness, inout half4 normal, inout half4 emission)
		{
			fixed4 packedMapA = tex2D (_PackedMapA, IN.uv_PackedMapA);

			diffuse.a = o.Alpha * _ContributionDiffuseSpecular;
			specSmoothness.a = o.Alpha * _ContributionDiffuseSpecular;
			normal.a = packedMapA.x * _ContributionNormal; 
			emission.a = o.Alpha * _ContributionDiffuseSpecular;
		}

		ENDCG

		Blend One One
		ColorMask A

		CGPROGRAM

		#pragma surface surf Standard finalgbuffer:DecalFinalGBuffer exclude_path:forward exclude_path:prepass noshadow noforwardadd keepalpha
		#pragma target 3.0

		sampler2D _PackedMapA;
		sampler2D _PackedMapB;
		sampler2D _BumpMap;

		struct Input 
		{
			float2 uv_PackedMapA; 
			float3 viewDir;
		};

		float _ParallaxHeight;
		half _SmoothnessLow;
		half _SmoothnessHigh;

		half _ContributionDiffuseSpecular;

		half _DirtIntensity;
		half _DirtMaskAORemappingMin;
		half _DirtMaskAORemappingMax;

		inline half3 RemapColorToRange (half3 f, half min, half max)
		{
			f = saturate (f);
			f = (f - min) / (max - min);
			return f;
		}

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			half h = tex2D (_PackedMapB, IN.uv_PackedMapA).w;
			float2 offset = ParallaxOffset (h, _ParallaxHeight, IN.viewDir);
			IN.uv_PackedMapA += offset;

			fixed4 packedMapA = tex2D (_PackedMapA, IN.uv_PackedMapA);
			fixed4 packedMapB = tex2D (_PackedMapB, IN.uv_PackedMapA);
			fixed3 normal = UnpackNormal (tex2D (_BumpMap, IN.uv_PackedMapA));

			half remappedOcclusion = RemapColorToRange (packedMapA.w, _DirtMaskAORemappingMin, _DirtMaskAORemappingMax);
			half dirtMask = saturate (packedMapA.y + (1 - remappedOcclusion));

			o.Alpha = packedMapA.x;
			o.Normal = normal;
			o.Smoothness = lerp (lerp (0, _SmoothnessHigh, packedMapA.z), saturate (lerp (_SmoothnessLow, _SmoothnessHigh, packedMapA.z)), dirtMask);
		}

		void DecalFinalGBuffer (Input IN, SurfaceOutputStandard o, inout half4 diffuse, inout half4 specSmoothness, inout half4 normal, inout half4 emission)
		{
			specSmoothness.a *= o.Alpha * _ContributionDiffuseSpecular;
		}

		ENDCG
	} 
	FallBack "Diffuse"
}






/*

// Old alpha test based version

Shader "Hardsurface/DecalDeferredSimple" 
{
	Properties 
	{
		_MainTex ("Emission mask (R)/Smoothness mask (G)/AO (B)/Alpha (A)", 2D) = "white" {}
		_BumpMap ("Normal (RGB)", 2D) = "bump" {}

		_ColorMain ("Dielectric albedo/Metal specular", Color) = (1,1,1,1)
		_ColorEmission ("Emission color", Color) = (1,1,1,1)

		_SmoothnessLow ("Smoothness (Low)", Range(0,1)) = 0.5
		_SmoothnessHigh ("Smoothness (High)", Range(0,1)) = 0.5
		_Metalness ("Metalness", Range(0,1)) = 0.0
		_Cutoff ("Cutoff", Range(0.001,1)) = 0.5

		_ContributionDiffuseSpecular ("Blend diffuse/specular", Range(0,1)) = 0.0
		_ContributionSmoothness ("Blend smoothness", Range(0,1)) = 0.0
		_ContributionNormal ("Blend normals", Range(0,1)) = 1.0
		_ContributionEmission ("Blend emission from mask", Range(0,1)) = 0.0
		_ContributionEmissionMaskAO ("Restrict emission by AO", Range(0,1)) = 0.0
		_ContributionOcclusion ("Blend AO", Range(0,1)) = 1.0
		_ContributionCavity ("Use AO as cavity", Range(0,1)) = 1.0
	}
	SubShader 
	{
		Tags {"Queue"="AlphaTest" "IgnoreProjector"="True" "RenderType"="Opaque" "ForceNoShadowCasting"="True"}
		LOD 300
		Offset -1, -1

		Blend One OneMinusSrcAlpha

		CGPROGRAM

		#pragma surface surf Standard finalgbuffer:DecalFinalGBuffer exclude_path:forward exclude_path:prepass noshadow noforwardadd keepalpha
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _BumpMap;

		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_BumpMap;
		};

		fixed3 _ColorMain;
		fixed3 _ColorEmission;
		half _Metalness;

		half _ContributionDiffuseSpecular;
		half _ContributionNormal;
		half _ContributionEmission;
		half _ContributionEmissionMaskAO;
		half _ContributionOcclusion;
		half _ContributionCavity;

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			fixed4 main = tex2D(_MainTex, IN.uv_MainTex);
			fixed3 normal = UnpackNormal(tex2D(_BumpMap, IN.uv_BumpMap));

			o.Albedo = _ColorMain * lerp (1, main.z, _ContributionCavity);
			o.Normal = normal;
			o.Metallic = _Metalness;
			o.Smoothness = 1; // we will output it in a second pass, for now we need it at 1.0 to blend specular properly
			o.Occlusion = lerp (1, main.z, _ContributionOcclusion);
			o.Alpha = main.w;
			o.Emission = lerp (0, main.x * lerp (_ColorEmission, half3 (1,1,1), main.x / 2 + 0.5), _ContributionEmission);
		}

		void DecalFinalGBuffer (Input IN, SurfaceOutputStandard o, inout half4 diffuse, inout half4 specSmoothness, inout half4 normal, inout half4 emission)
		{
			float emissionMultiplier = max (max (_ContributionNormal, _ContributionDiffuseSpecular), max (_ContributionOcclusion, _ContributionCavity));
			float emissionMask = lerp (1, 1 - o.Occlusion, _ContributionEmissionMaskAO * ((1 - _ContributionEmission) * o.Emission));

			diffuse *= o.Alpha * _ContributionDiffuseSpecular;
			specSmoothness *= o.Alpha * _ContributionDiffuseSpecular;
			normal *= o.Alpha * _ContributionNormal; 
			emission *= o.Alpha * emissionMask * emissionMultiplier;
		}

		ENDCG

		Blend One Zero
		ColorMask A

		CGPROGRAM

		#pragma surface surf Standard exclude_path:forward exclude_path:prepass noshadow noforwardadd keepalpha
		#pragma target 3.0

		sampler2D _MainTex;

		struct Input 
		{
			float2 uv_MainTex;
		};

		half _SmoothnessLow;
		half _SmoothnessHigh;
		half _ContributionSmoothness;
		half _Cutoff;

		void surf (Input IN, inout SurfaceOutputStandard o) 
		{
			fixed4 main = tex2D(_MainTex, IN.uv_MainTex);
			o.Smoothness = saturate (lerp (_SmoothnessLow, _SmoothnessHigh, main.y));
			clip (main.w * _ContributionSmoothness - _Cutoff);
		}

		ENDCG
	} 
	FallBack "Diffuse"
}
*/




